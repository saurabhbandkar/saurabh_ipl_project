# Steps to run the app
1. Clone the app from the Gitlab repository
2. Open the local folder of the app
3. Initialize npm by using the command npm init
4. Install the dependencies using command npm install
5. Change directory to ./src/server/
5. Start the app by using the command node index.js
6. Output will be saved in ../public/output/ folder after the runtime of the app which is approximately 12 seconds.
