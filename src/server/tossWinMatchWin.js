function outputTossWinMatchWin() {

    const fs = require('fs');
    const csv = require('csv-parser');
    const matchesData = new Array();
    let inputFilePath = '../data/matches.csv';

    fs.createReadStream(inputFilePath)
        .on('error', () => {
            console.error('Input file does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => {
            matchesData.push(data);
        })
        .on('end', () => {
            function tossWinMatchWin(dataSet) {

                let teams = new Object();

                for (let iterator = 0; iterator < dataSet.length; iterator++) {
                    teams[dataSet[iterator]['team1']] = 0;
                    teams[dataSet[iterator]['team2']] = 0;
                };
                for (let iterator = 0; iterator < dataSet.length; iterator++) {
                    if (dataSet[iterator]['toss_winner'] == dataSet[iterator]['winner']) {
                        teams[dataSet[iterator]['winner']] += 1;
                    };
                };
                function saveFile(output) {

                    const outputData = JSON.stringify(output);
                    let outputFile = '../public/output/tossWinMatchWin.json';

                    fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('Successfully created file tossWinMatchWin.json');
                        };
                    });
                };
                saveFile(teams);
            }
            tossWinMatchWin(matchesData);
        });
};

module.exports = { outputTossWinMatchWin };


