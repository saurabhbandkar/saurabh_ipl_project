function outputMatchesPerYear() {

    const csv = require('csv-parser');
    const fs = require('fs');
    let matchesData = [];
    let inputFilePath = '../data/matches.csv';

    fs.createReadStream(inputFilePath)
        .on('error', () => {
            console.error('Input file does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {

            function matchesPerYear(dataSet) {

                let matchesCount = new Object();
                let years = new Array();
                let allYears = new Array();

                for (let index = 0; index < dataSet.length; index++) {

                    let currentYear = dataSet[index]['season'];

                    allYears.push(currentYear);
                    if (!years.includes(currentYear)) {
                        years.push(currentYear);
                    };
                };
                for (let index = 0; index < years.length; index++) {

                    let counter = 0;

                    for (let iterator = 0; iterator < allYears.length; iterator++) {
                        if (years[index] === allYears[iterator]) {
                            counter += 1;
                        };
                    };
                    matchesCount[years[index]] = counter;
                };
                function saveFile(output) {

                    const outputData = JSON.stringify(output);
                    let outputFile = "../public/output/matchesPerYear.json";

                    fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('Successfully created file matchesPerYear.json');
                        };                        
                    });
                };
                saveFile(matchesCount);
            };
            matchesPerYear(matchesData);
        });
};
module.exports = { outputMatchesPerYear };