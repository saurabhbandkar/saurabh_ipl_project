function outputMatchesWonPerTeamPerYear() {

    const csv = require('csv-parser');
    const fs = require('fs');
    let matchesData = new Array();
    let inputFilePath = '../data/matches.csv';
    fs.createReadStream(inputFilePath)
        .on('error', () => {
            console.error('Input file does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))

        .on('end', () => {

            function matchesPerTeamPerYear(dataSet) {

                let matchesCount = new Object();
                let years = new Array();
                let teamsList = new Array();
                let allYears = new Array();

                for (let index = 0; index < dataSet.length; index++) {

                    let currentYear = dataSet[index]['season'];
                    let team1 = dataSet[index]['team1'];
                    let team2 = dataSet[index]['team2'];

                    if (!teamsList.includes(team1)) {
                        teamsList.push(team1);
                    };
                    if (!teamsList.includes(team2)) {
                        teamsList.push(team2);
                    };
                    if (!years.includes(currentYear)) {
                        years.push(currentYear);
                    };
                };
                for (let teamIndex = 0; teamIndex < teamsList.length; teamIndex++) {

                    let result = new Array();

                    for (let year = 0; year < years.length; year++) {

                        let counter = 0;

                        for (let iterator = 0; iterator < dataSet.length; iterator++) {

                            let currentYear = dataSet[iterator]['season'];
                            let currentWinner = dataSet[iterator]['winner'];

                            if (currentYear == years[year] && currentWinner == teamsList[teamIndex]) {
                                counter += 1;
                            };

                        };
                        result.push([years[year], counter]);
                    };
                    matchesCount[teamsList[teamIndex]] = result;
                };
                function saveFile(output) {

                    const outputData = JSON.stringify(output);
                    let outputFile = "../public/output/matchesPerTeamPerYear.json";

                    fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('Successfully created file matchesPerTeamPerYear.json');
                        };                      
                    });
                };
                saveFile(matchesCount);
            };
            matchesPerTeamPerYear(matchesData);
        });
};
module.exports = { outputMatchesWonPerTeamPerYear };