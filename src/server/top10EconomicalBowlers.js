function outputTop10EconomicalBowlers() {

    const csv = require('csv-parser');
    const fs = require('fs');
    let deliveriesData = new Array();
    let matchesData = new Array();
    let matchesInputFilePath = '../data/matches.csv';
    let deliveriesInputFilePath = '../data/deliveries.csv';

    fs.createReadStream(matchesInputFilePath)
        .on('error', () => {
            console.error('Input file matches does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {

            function getYear(dataSet1) {

                let yearId = new Object();
                let year = new Array();

                for (let indexMatches = 0; indexMatches < dataSet1.length; indexMatches++) {
                    if (!year.includes(dataSet1[indexMatches]['season'])) {
                        year.push(dataSet1[indexMatches]['season']);
                    };
                };
                for (let yearIndex = 0; yearIndex < year.length; yearIndex++) {

                    let id = new Array();

                    for (let iterator = 0; iterator < dataSet1.length; iterator++) {

                        let currentYear = year[yearIndex];
                        let currentDataBaseYear = dataSet1[iterator]['season'];

                        if (currentYear == currentDataBaseYear) {
                            id.push(dataSet1[iterator]['id']);
                        };
                    };
                    yearId[year[yearIndex]] = id;
                };
                fs.createReadStream(deliveriesInputFilePath)
                    .on('error', () => {
                        console.error('Input file deliveries does not exist!!!');
                    })
                    .pipe(csv())
                    .on('data', (data) => deliveriesData.push(data))
                    .on('end', () => {

                        function economyRate(dataSet) {

                            let bowlersList = new Array();

                            for (let indexDeliveries = 0; indexDeliveries < dataSet.length; indexDeliveries++) {

                                let currentBowler = dataSet[indexDeliveries]['bowler'];

                                if (!bowlersList.includes(currentBowler)) {
                                    bowlersList.push(currentBowler);
                                };
                            };

                            const bowlerEconomy = new Array();

                            for (let bowlerIndex = 0; bowlerIndex < bowlersList.length; bowlerIndex++) {

                                let currentBowler = bowlersList[bowlerIndex];
                                let economy = 0;
                                let overs = 0;
                                let runs = 0;

                                for (let iteratorDeliveries = 0; iteratorDeliveries < dataSet.length; iteratorDeliveries++) {

                                    let currentDataBaseBowler = dataSet[iteratorDeliveries]['bowler'];
                                    let currentId = dataSet[iteratorDeliveries]['match_id'];
                                    let currentWides = dataSet[iteratorDeliveries]['wide_runs'];
                                    let currentNoBalls = dataSet[iteratorDeliveries]['noball_runs'];
                                    let currentBatsmen = dataSet[iteratorDeliveries]['batsman_runs'];

                                    if (yearId[2015].includes(currentId)) {
                                        if (currentBowler == currentDataBaseBowler) {
                                            runs = runs + parseInt(currentWides) + parseInt(currentNoBalls) + parseInt(currentBatsmen);
                                        };
                                        if (dataSet.length - iteratorDeliveries !== 1) {
                                            if (currentBowler == currentDataBaseBowler && currentDataBaseBowler !== dataSet[iteratorDeliveries + 1]['bowler']) {
                                                overs += 1;
                                            };
                                        };
                                    };
                                };
                                if (overs > 0) {
                                    economy = runs / overs;
                                    bowlerEconomy.push([currentBowler, parseFloat(economy.toFixed(2))]);
                                };
                            };
                            bowlerEconomy.sort(function (x, y) {
                                return x[1] - y[1];
                            });

                            let top10BowlerEconomy = bowlerEconomy.slice(0, 10);

                            function saveFile(output) {

                                const outputData = JSON.stringify(output);
                                let outputFile = "../public/output/bowlerEconomy.json";

                                fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                                    if (err) {
                                        console.error(err);
                                    }
                                    else {
                                        console.log('Successfully created file bowlerEconomy.json');
                                    };
                                });
                            };
                            saveFile(top10BowlerEconomy);
                        };
                        economyRate(deliveriesData);
                    });
            };
            getYear(matchesData);
        });
};
module.exports = { outputTop10EconomicalBowlers };
