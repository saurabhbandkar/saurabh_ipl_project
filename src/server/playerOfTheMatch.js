function outputPlayerOfTheMatch() {
    const fs = require('fs');
    const csv = require('csv-parser');
    const years = new Object();
    const player = new Object();
    const matchesData = new Array();
    let inputFilePath = '../data/matches.csv';

    fs.createReadStream(inputFilePath)
        .on('error', () => {
            console.error('Input file does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => { matchesData.push(data) })
        .on('end', () => {
            function playerOfTheMatch(dataSet) {
                for (let iterator = 0; iterator < dataSet.length; iterator++) {
                    years[dataSet[iterator]['season']] = null;
                    player[dataSet[iterator]['player_of_match']] = 0;
                };
                for (year in years) {

                    let selectedPlayer = '';

                    for (let iterator = 0; iterator < dataSet.length; iterator++) {

                        let currentYear = dataSet[iterator]['season'];

                        if (currentYear == year) {
                            player[dataSet[iterator]['player_of_match']] += 1;
                        };
                    };

                    let mostPlayerOfTheMatch = { '': 0 };

                    for (currentPlayer in player) {
                        if (Object.values(mostPlayerOfTheMatch) < player[currentPlayer]) {
                            mostPlayerOfTheMatch = { [currentPlayer]: player[currentPlayer] };
                        };
                    };
                    years[year] = mostPlayerOfTheMatch;
                    for (let playerName in player) {
                        player[playerName] = 0;
                    };
                };

                function saveFile(output) {

                    let outputData = JSON.stringify(output);
                    let outputFile = '../public/output/playerOfTheMatch.json';

                    fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                        if (err) {
                            console.error(err);
                        }
                        else {
                            console.log('Successfully created file playerOfTheMatch.json');
                        };                        
                    });
                };
                saveFile(years);
            };
            playerOfTheMatch(matchesData);
        });
};

module.exports = { outputPlayerOfTheMatch };