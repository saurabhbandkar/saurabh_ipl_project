function outputRunsConcededPerYear() {
    const csv = require('csv-parser');
    const fs = require('fs');
    const deliveriesData = new Array();
    const matchesData = new Array();
    let matchesInputFilePath = '../data/matches.csv';
    let deliveriesInputFilePath = '../data/deliveries.csv';

    fs.createReadStream(matchesInputFilePath)
        .on('error', () => {
            console.error('Input file matches does not exist!!!');
        })
        .pipe(csv())
        .on('data', (data) => matchesData.push(data))
        .on('end', () => {

            function getYear(dataSet1) {

                let yearId = new Object();
                let year = new Array();

                for (let indexMatches = 0; indexMatches < dataSet1.length; indexMatches++) {
                    if (!year.includes(dataSet1[indexMatches]['season'])) {
                        year.push(dataSet1[indexMatches]['season']);
                    };
                };
                for (let yearIndex = 0; yearIndex < year.length; yearIndex++) {

                    let id = new Array();

                    for (let iterator = 0; iterator < dataSet1.length; iterator++) {

                        let currentYear = year[yearIndex];
                        let currentDataBaseYear = dataSet1[iterator]['season'];

                        if (currentYear == currentDataBaseYear) {
                            id.push(dataSet1[iterator]['id']);
                        }
                    }
                    yearId[year[yearIndex]] = id;
                }

                let teams = new Array();

                for (let indexMatchesTeams = 0; indexMatchesTeams < dataSet1.length; indexMatchesTeams++) {

                    let team1 = dataSet1[indexMatchesTeams]['team1'];
                    let team2 = dataSet1[indexMatchesTeams]['team2'];

                    if (!teams.includes(team1)) {
                        teams.push(team1);
                    };
                    if (!teams.includes(team2)) {
                        teams.push(team2);
                    };
                };

                fs.createReadStream(deliveriesInputFilePath)
                    .on('error', () => {
                        console.error('Input file matches does not exist!!!');
                    })
                    .pipe(csv())
                    .on('data', (data) => deliveriesData.push(data))
                    .on('end', () => {

                        function extraRunsPerTeam(dataSet) {

                            let teamExtraRuns = new Object();

                            for (let teamIndex = 0; teamIndex < teams.length; teamIndex++) {

                                let extraRuns = 0;

                                for (let indexDeliveries = 0; indexDeliveries < dataSet.length; indexDeliveries++) {

                                    let currentTeam = dataSet[indexDeliveries]['bowling_team'];
                                    let currentId = dataSet[indexDeliveries]['match_id'];
                                    let currentExtras = dataSet[indexDeliveries]['extra_runs'];

                                    if (yearId[2016].includes(currentId) && currentTeam == teams[teamIndex]) {
                                        extraRuns = extraRuns + parseInt(currentExtras);
                                    };
                                };
                                teamExtraRuns[teams[teamIndex]] = extraRuns;
                            };
                            function saveFile(output) {

                                const outputData = JSON.stringify(output);
                                let outputFile = "../public/output/extraRunsPerTeam.json";

                                fs.writeFile(outputFile, outputData, 'utf8', function (err) {
                                    if (err) {
                                        console.error(err);
                                    }
                                    else {
                                        console.log('Successfully created file extraRunsPerTeam.json');
                                    };
                                });
                            };
                            saveFile(teamExtraRuns);
                        };
                        extraRunsPerTeam(deliveriesData);
                    });
            };
            getYear(matchesData);
        });
};
module.exports = { outputRunsConcededPerYear };